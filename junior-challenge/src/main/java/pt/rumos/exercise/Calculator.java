package pt.rumos.exercise;

public class Calculator {

    /**
     * This method receives two integers and returns the sum of them in order.
     * 
     * @param firstValue
     * @param secondValue
     * @return firstValue + secondValue
     */
    public int add(int firstValue, int secondValue) {
	return firstValue + secondValue;
    }
}
