package pt.rumos.exercise;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculatorTest {
    
    private static final Calculator calc = new Calculator();
    
    @Test
    public void CalculatorAddIntegerTest() {
	int firstValue = 5;
	int secondValue = 12;
	int expected = 17;
	int actual = calc.add(firstValue, secondValue);
	assertEquals(expected, actual);
    }

}
